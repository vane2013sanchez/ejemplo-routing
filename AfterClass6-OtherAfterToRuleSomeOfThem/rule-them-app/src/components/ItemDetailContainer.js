import React, {useEffect, useState}from 'react'
import {ItemDetail} from './itemDetail'
import {useParams} from 'react-router-dom'

const datos = [ 
    { id:1,  categoria: 'desafio',title:'Remeras-desafio', description:'Remeras Rojas',        price:25,  pictureUrl:'' },
    { id:23, categoria: 'after',title:'Pantalon-after', description:'Pantalon de jean',    price:35,  pictureUrl:'' },
    { id:78,categoria: 'after' ,title:'Medias-after', description:'Medias con dibujitos',  price:500, pictureUrl:'' },
    { id:14,  categoria: 'entrega',title:'Remeras-entrega', description:'Remeras Rojas',        price:25,  pictureUrl:'' },
    { id:243, categoria: 'entrega',title:'Pantalon-entrega', description:'Pantalon de jean',    price:35,  pictureUrl:'' },
    { id:748,categoria: 'entrega' ,title:'Medias-entrega', description:'Medias con dibujitos',  price:500, pictureUrl:'' },
]


const getItems = () => { 
    return new Promise((trajoDatosOk,error)=>{
        setTimeout(()=>{
            trajoDatosOk(datos)
        },2000)
    })

}


export function ItemDetailContainer() {
    const [datosDelItem, setDatosDelItem] = useState({});
    const {itemId}= useParams()

    useEffect(() => {

        getItems()
        .then((datos)=>{
            setDatosDelItem(datos)
        })

    }, [])   

 return <div>
     <h2>{}</h2>
 <ItemDetail item={datosDelItem} />
 </div>

}
