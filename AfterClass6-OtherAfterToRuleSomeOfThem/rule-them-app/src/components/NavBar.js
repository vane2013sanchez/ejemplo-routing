import React from 'react'
import {CartWidget} from './CartWidget';
import {Link} from 'react-router-dom'

export const NavBar = () =>{

return <>
<div style={{border:'solid red'}}>
     <Link to='/'><h1>El Titulo</h1></Link>
        {/* Es un Comentario */}
        <nav>
            <ul>
            <Link to='/category/desafio'>  <li>Desafios</li></Link>
            <Link to='/category/entrega'>  <li>Entregas</li></Link>
            <Link to='/category/after'>  <li>AfterClass</li></Link>
            </ul>
        </nav>
        <CartWidget />
</div>
    </>;
}

export default NavBar