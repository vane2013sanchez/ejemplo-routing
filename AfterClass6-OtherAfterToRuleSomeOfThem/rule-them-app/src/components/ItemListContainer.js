import React, { useEffect,useState } from 'react'
import { ItemCount } from './ItemCount'
import { ItemList } from './ItemList'
import {useParams} from 'react-router-dom'

const datos = [ 
    { id:1,  categoria: 'desafio',title:'Remeras-desafio', description:'Remeras Rojas',        price:25,  pictureUrl:'' },
    { id:23, categoria: 'after',title:'Pantalon-after', description:'Pantalon de jean',    price:35,  pictureUrl:'' },
    { id:78,categoria: 'after' ,title:'Medias-after', description:'Medias con dibujitos',  price:500, pictureUrl:'' },
    { id:14,  categoria: 'entrega',title:'Remeras-entrega', description:'Remeras Rojas',        price:25,  pictureUrl:'' },
    { id:243, categoria: 'entrega',title:'Pantalon-entrega', description:'Pantalon de jean',    price:35,  pictureUrl:'' },
    { id:748,categoria: 'entrega' ,title:'Medias-entrega', description:'Medias con dibujitos',  price:500, pictureUrl:'' },
]

export const ItemListContainer = ({greeting:saludo, a ,   b:otrab }) => {
    const [resultadoDeItems, setResultadoDeItems] = useState(null)
    const {categoryId}= useParams()

    useEffect(()=>{
        const llamadaAlServidor = new Promise((trajoDatosOk,error)=>{
            setTimeout(()=>{
                categoryId ?
                trajoDatosOk(datos.filter(e=>e.categoria === categoryId))
                : 
                trajoDatosOk(datos)
            },2000)
        })

        
        llamadaAlServidor.then((datos) =>{
            setResultadoDeItems(datos)
        })

    },[categoryId])

    const seAgrego = (num)=>{console.log('se agrego un item',num)};
    
    return (
        <div>
            <h4>{categoryId}</h4>

            { resultadoDeItems  
            ? <ItemList items={resultadoDeItems}  /> 
            : null}

            <div>
                <ItemCount initial={11} stock={10} onAdd={seAgrego}/>
            </div>
        </div>
    )
}
