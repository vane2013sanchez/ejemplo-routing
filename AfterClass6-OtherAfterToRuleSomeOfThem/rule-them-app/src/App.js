import React from "react";
import "./App.css";

import NavBar from "./components/NavBar";

import { ItemListContainer } from "./components/ItemListContainer";
import { ItemDetailContainer } from "./components/ItemDetailContainer";
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";

function App() {
  return (
    <>
      <BrowserRouter>
        <h2>Nav Bar</h2>
        <NavBar />
        <Switch>
          <Route exact path="/">
            <h2>Item List</h2>
            <ItemListContainer greeting="Hola Coders" a={4} b="chau" />
          </Route>
          <Route path="/category/:categoryId">
            <h2>Item List</h2>
            <ItemListContainer greeting="Hola Coders" a={4} b="chau" />
          </Route>
          <Route path="/item/:itemId">
                <h2>Item Detail</h2>
            <ItemDetailContainer></ItemDetailContainer>
          </Route>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
